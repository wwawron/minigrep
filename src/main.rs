use std::env;
use std::process;

use minigrep::Config;

fn main() {
    // args jest podawane przy wywoływaniu cargo run
    // pierwszy argument to szukany wzór, drugi to plik
    
    let config = Config::new(env::args()).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    if let Err(e) = minigrep::run(config) {
        eprintln!("Application error: {}", e);

        process::exit(1);
    }
}

